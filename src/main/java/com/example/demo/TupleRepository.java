package com.example.demo;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "tuple", path = "tuples", exported = false)
public interface TupleRepository extends PagingAndSortingRepository<Tuple, Long>, JpaSpecificationExecutor<Tuple> {}