package com.example.demo;

import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RepositoryRestController()
@RequestMapping("api/tuples")
public class TupleController {


    @GetMapping("test")
	public ResponseEntity<Object> test() {

    	return ResponseEntity.ok("Hello, World!");
    }
}